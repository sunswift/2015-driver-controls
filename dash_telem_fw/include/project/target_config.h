//Test LEDs

#define RED_LED_PORT            2     
#define RED_LED_BIT             11     
#define YELLOW_LED_PORT         2    
#define YELLOW_LED_BIT          4     

//Signal LEDs

#define RIGHT_INDICATOR_PORT	2
#define RIGHT_INDICATOR_PIN		8

#define LEFT_INDICATOR_PORT		2
#define LEFT_INDICATOR_PIN		1

#define BACKLIGHT_ONE_PORT		1
#define BACKLIGHT_ONE_PIN		8

#define PARK_LED_PORT			1
#define PARK_LED_PIN			1

#define REVERSE_LED_PORT		1
#define REVERSE_LED_PIN			4

#define NTRL_LED_PORT			0
#define NTRL_LED_PIN			5

#define NTRL_PULLUP_LED_PORT	3
#define NTRL_PULLUP_LED_PIN		2

#define DRIVE_LED_PORT			0
#define DRIVE_LED_PIN			4

#define DRIVE_PULLUP_LED_PORT	3
#define DRIVE_PULLUP_LED_PIN	3

#define ECO_LED_PORT			0
#define ECO_LED_PIN				6

#define CRUISE_TOGGLE_LED_PORT	1
#define CRUISE_TOGGLE_LED_PIN	9

#define HANDBRAKE_LED_PORT		2
#define HANDBRAKE_LED_PIN		9

#define CHECK_LED_PORT			2
#define CHECK_LED_PIN			5

#define BRAKE_FAIL_PORT			2
#define BRAKE_FAIL_PIN			2

#define HEADLIGHT_FAILURE_LED_PORT		0
#define HEADLIGHT_FAILURE_LED_PIN		7

#define CTRL_LED_PORT			0
#define CTRL_LED_PIN			2

#define WARNING_LED_PORT		2
#define WARNING_LED_PIN			10

#define LOWBATT_PORT			0
#define LOWBATT_PIN				9

#define POS_LED_PORT			0
#define POS_LED_PIN				8

#define LOWBEAM_PORT			0
#define LOWBEAM_PIN				10

#define HIGHBEAM_PORT			1
#define HIGHBEAM_PIN			2

#define SEATBELT_LED_PORT		0
#define SEATBELT_LED_PIN		11

//Servo

#define SERVO_PORT	1
#define SERVO_PIN	3

// Shift registers

//Fuel
#define FUEL_DATA_PORT		3
#define FUEL_DATA_PIN		1

#define FUEL_RCLK_PORT		2
#define FUEL_RCLK_PIN		0

#define FUEL_SRCLK_PORT		2
#define FUEL_SRCLK_PIN		3

//Speed 
#define SPEED_DATA_PORT		2
#define SPEED_DATA_PIN		7

#define SPEED_RCLK_PORT		1
#define SPEED_RCLK_PIN		11

#define SPEED_SRCLK_PORT	1
#define SPEED_SRCLK_PIN		0

//Cruise
#define CRUISE_DATA_PORT	1
#define CRUISE_DATA_PIN		5

#define CRUISE_RCLK_PORT	2
#define CRUISE_RCLK_PIN		6

#define CRUISE_SRCLK_PORT	3
#define CRUISE_SRCLK_PIN	0



// Offsets for 32 bit flags for communicating between center/dash
#define OFFSET_ONOFF				0x1		// Enable (1) or disable (0)

#define OFFSET_INDICATOR_LEFT		0x2
#define OFFSET_INDICATOR_RIGHT		0x4

#define OFFSET_PARKED				0x8
#define OFFSET_REVERSE				0x10
#define OFFSET_NEUTRAL				0x20
#define OFFSET_DRIVE				0x40
#define OFFSET_ECO					0x80
#define OFFSET_CRUISE_TOGGLE		0x100

#define OFFSET_HANDBRAKE			0x200
#define OFFSET_HANDCHECK			0x400
#define OFFSET_BRAKE_FAIL			0x800
#define OFFSET_HEADLIGHT_FAIL		0x1000
#define OFFSET_CONTROL_FAILURE		0x2000
#define OFFSET_WARNING_NONSPECIFIC	0x4000

#define OFFSET_LOW_BATTERY			0x8000
#define OFFSET_POSITION_LIGHT		0x10000
#define OFFSET_LOWBEAM				0x20000
#define OFFSET_HIGHBEAM				0x40000
#define OFFSET_SEATBELT_SENSOR		0x80000

#define OFFSET_LOW_FLUID			0x100000
#define OFFSET_HORN					0x200000
#define OFFSET_HAZARDS				0x400000
#define OFFSET_DEMISTER				0x800000
#define OFFSET_WIPER_INT			0x1000000
#define OFFSET_WIPER_LOW			0x2000000
#define OFFSET_WIPER_HIGH			0x4000000
#define OFFSET_WIPER_WASH			0x8000000






// Command mapping for communication with other CAN nodes (Davina)
#define CHANNEL_CONTROLS_DRIVE           1

#define OFFSET_PARKED_TEMP             0x2
#define OFFSET_NEUTRAL_TEMP             0x4
#define OFFSET_REVERSE_TEMP             0x8
#define OFFSET_DRIVE_TEMP               0x10
#define OFFSET_ECO_TEMP                 0x20
