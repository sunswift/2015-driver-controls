/* --------------------------------------------------------------------------
    Dash
    File name: dash.c
    Author: Stephanie Ascone
    Description: Dash
   -------------------------------------------------------------------------- */
/*
 * This file is part of the Sunswift Template project
 *
 * This tempalte is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * It is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with the project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <scandal/wavesculptor.h>
#include <scandal/engine.h>
#include <scandal/message.h>
#include <scandal/leds.h>
#include <scandal/utils.h>
#include <scandal/uart.h>
#include <scandal/stdio.h>
#include <scandal/wdt.h>
#include <scandal/error.h>

#include <string.h>

#if defined(lpc11c14) || defined(lpc1768)
#include <project/driver_config.h>
#include <project/target_config.h>
#include <arch/adc.h>
#include <arch/ssp.h>
#include <arch/can.h>
#include <arch/uart.h>
#include <arch/timer.h>
#include <arch/gpio.h>
#include <arch/types.h>
#include <arch/i2c.h>
#endif // lpc11c14 || lpc1768

#define INDICATOR_PERIOD	 	660

void init_LEDS(void);
void LEDS_on(void);
void debug_LEDS(void);
void temp_seatbelt(void);
void clear_drive_leds(void);
void toggle_backlights(int on_off);

// Indicator enable (as in enable them to flash)
uint8_t indicator_left_en = 0;
uint8_t indicator_right_en = 0;
int hazard_state = 0;
// Indicator on (as in the light itself is on)
int indicators_on = 0;
sc_time_t indicator_time = 0;


void LED_in_channel_handler(int32_t value, uint32_t src_time){
	/* On/off value is first bit of value. Drive state is always taken as on
	 * as they are exclusive (cannot be in drive and neutral)
	 */
	// Get the turn on/off value
	UART_printf("LED Handler got called with value: %d\n\r", value);
    int on_off = value & OFFSET_ONOFF;

    // Bit clear the on/off value
	value &= ~OFFSET_ONOFF;
	//UART_printf("Value is %x\n\r", value);
	switch(value){
		// INDICATORs are enabled so they can be toggled at a set rate
		case OFFSET_INDICATOR_LEFT:
			UART_printf("Left Ind State = %d \n\r", on_off);
			indicator_left_en = on_off;
			if(!on_off) {
				indicator_right_en = on_off;
			}
			break;
		case OFFSET_INDICATOR_RIGHT:
			UART_printf("Right Ind State = %d \n\r", on_off);
			indicator_right_en = on_off;
			if(!on_off) {
				indicator_left_en = on_off;
			}
			break;
		case OFFSET_HAZARDS:
			UART_printf("Hazard state toggle\n");
			hazard_state = ~hazard_state;

		// Normal toggled LEDs
		case OFFSET_POSITION_LIGHT:
			UART_printf("Parking light state = %d \n\r", on_off);
			GPIO_SetValue(POS_LED_PORT, POS_LED_PIN, on_off);
			break;
		case OFFSET_LOWBEAM:
			UART_printf("Low beam State = %d \n\r", on_off);
			GPIO_SetValue(LOWBEAM_PORT, LOWBEAM_PIN, on_off);
			GPIO_SetValue(HIGHBEAM_PORT, HIGHBEAM_PIN, 0);
			//value = OFFSET_POSITION_LIGHT;
			GPIO_SetValue(POS_LED_PORT, POS_LED_PIN, 0);
			//toggle_backlights(on_off);
			if (on_off==0){
				GPIO_SetValue(POS_LED_PORT, POS_LED_PIN, 1);
			}
			break;
		case OFFSET_HIGHBEAM:
			UART_printf("High beam State = %d \n\r", on_off);
			GPIO_SetValue(HIGHBEAM_PORT, HIGHBEAM_PIN, on_off);
			GPIO_SetValue(LOWBEAM_PORT, LOWBEAM_PIN, 0);
			//GPIO_SetValue(POS_LED_PORT, POS_LED_PIN, 0);
			//toggle_backlights(on_off);
			if (on_off==0){
				GPIO_SetValue(LOWBEAM_PORT, LOWBEAM_PIN, 1);
			}
			break;
		case OFFSET_HANDBRAKE:
			UART_printf("Handbrake state = %d \n\r", on_off);
			GPIO_SetValue(HANDBRAKE_LED_PORT, HANDBRAKE_LED_PIN, on_off);
			break;
		case OFFSET_HANDCHECK:
			UART_printf("Handcheck state = %d \n\r", on_off);
			GPIO_SetValue(CHECK_LED_PORT, CHECK_LED_PIN, on_off);
			break;
		case OFFSET_BRAKE_FAIL:
			UART_printf("Brake fail state = %d \n\r", on_off);
			GPIO_SetValue(BRAKE_FAIL_PORT, BRAKE_FAIL_PIN, on_off);
			break;
		case OFFSET_CRUISE_TOGGLE:
			//GPIO_SetValue(BACKLIGHT_ONE_PORT, BACKLIGHT_ONE_PIN, on_off);
			break;
		case OFFSET_LOW_FLUID:
			UART_printf("Low fluid state = %d \n\r", on_off);
			GPIO_SetValue(WARNING_LED_PORT, WARNING_LED_PIN, on_off);
			break;
		case OFFSET_HEADLIGHT_FAIL:
			UART_printf("Lights fail State = %d\n\r", on_off);
			GPIO_SetValue(HEADLIGHT_FAILURE_LED_PORT, HEADLIGHT_FAILURE_LED_PIN, on_off);
			break;
		case OFFSET_CONTROL_FAILURE:
			UART_printf("Lights fail State = %d\n\r", on_off);
			GPIO_SetValue(CTRL_LED_PORT, CTRL_LED_PIN, on_off);
			break;
		case OFFSET_HORN:
			//GPIO_SetValue(BACKLIGHT_ONE_PORT, BACKLIGHT_ONE_PIN, on_off);
			break;
		// Drive state pins are exclusive, disable all others before enabling
		case OFFSET_PARKED:
			UART_printf("Parked\n");
			clear_drive_leds();
			GPIO_SetValue(PARK_LED_PORT, PARK_LED_PIN, 1);
			break;
		case OFFSET_NEUTRAL:
			UART_printf("Neutral\n");
			clear_drive_leds();
			GPIO_SetValue(NTRL_PULLUP_LED_PORT, NTRL_PULLUP_LED_PIN, 1);
			GPIO_SetValue(NTRL_LED_PORT, NTRL_LED_PIN, 1);
			break;
		case OFFSET_DRIVE:
			UART_printf("Drive\n");
			clear_drive_leds();
			GPIO_SetValue(DRIVE_PULLUP_LED_PORT, DRIVE_PULLUP_LED_PIN, 1);
			GPIO_SetValue(DRIVE_LED_PORT, DRIVE_LED_PIN, 1);
			break;
		case OFFSET_ECO:
			UART_printf("Economical\n");
			clear_drive_leds();
			GPIO_SetValue(ECO_LED_PORT, ECO_LED_PIN, 1);
			break;
		case OFFSET_REVERSE:
			UART_printf("Reverse\n");
			clear_drive_leds();
			GPIO_SetValue(REVERSE_LED_PORT, REVERSE_LED_PIN, 1);
			break;
		case OFFSET_LOW_BATTERY:
			UART_printf("Low Battery LED state = %d \n\r", on_off);
			GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, on_off);
			break;
		case OFFSET_SEATBELT_SENSOR:
			UART_printf("Belt LED state = %d", on_off);
			GPIO_SetValue(SEATBELT_LED_PORT, SEATBELT_LED_PIN, on_off);
			break;
		case OFFSET_WARNING_NONSPECIFIC:
			UART_printf("Low fluid/Warning non-specific state = %d \n\r", on_off);
			GPIO_SetValue(WARNING_LED_PORT, WARNING_LED_PIN, on_off);
		default: break;
	}

}

void hazard_handler(int32_t value, uint32_t src_time)
{
	UART_printf("Hazard toggled \n");
	hazard_state = ~hazard_state;
}

void on_handler(int32_t value, uint32_t src_time)
{
	GPIO_SetValue(3, 1, 1);
	UART_printf("on handler called\n");
}

void start_handler(int32_t value, uint32_t src_time)
{
	debug_LEDS();
	scandal_register_in_channel_handler(0, &LED_in_channel_handler);
	UART_printf("start handler called\n");
}

void setup(void)
{
	GPIO_Init();
	GPIO_SetFunction(DRIVE_LED_PORT, DRIVE_LED_PIN, GPIO_PIO, GPIO_MODE_NONE);
	GPIO_SetFunction(NTRL_LED_PORT, NTRL_LED_PIN, GPIO_PIO, GPIO_MODE_NONE);

	//Initialise Test LEDs
	GPIO_SetDir(RED_LED_PORT, RED_LED_BIT, 1);
	GPIO_SetDir(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);

    /* Set directions for all GPIO ports */
	init_LEDS();
	init_ShiftRegisters();

	//Initialise Servo
    //init_timer32PWM(1, 500000, MATCH2|MATCH2);
    //enable_timer32(1);
	//setMatch_timer32PWM (1, 2, 500000);

	GPIO_SetFunction(HIGHBEAM_PORT, HIGHBEAM_PIN, GPIO_PIO, GPIO_MODE_PULLUP);


	//TEMP SEATBELT LIGHT
	//GPIO_SetDir(SEATBELT_SENSOR_PORT, SEATBELT_SENSOR_PIN, 1);

    /* Write button interrupts in button_handler.c */
    //setup_button_interrupts();
    //seg_setup

} // setup

int main(void)
{
	setup();

	/* Initialise the watchdog timer. If the node crashes, it will restart automatically */
	WDT_Init();

	/* Initialise Scandal, registers for the config messages, timesync messages and in channels */
	scandal_init();

	/* Initialise UART0 */
	UART_Init(115200);

	/* Wait until UART is ready */
	scandal_delay(100);

	/* Display welcome header over UART */
	UART_printf("Welcome to the dashboard project! This is coming out over UART0\n\r");

	scandal_register_in_channel_handler(0, &LED_in_channel_handler);
	scandal_register_in_channel_handler(1, &hazard_handler);
	scandal_register_in_channel_handler(2, &on_handler);
	scandal_register_in_channel_handler(3, &start_handler);

	sc_time_t one_sec_timer = sc_get_timer();
	sc_time_t indicator_timer = sc_get_timer();

	debug_LEDS();
	toggle_backlights(1);

	/* This is the main loop, go for ever! */
	while (1) {
		/* This checks whether there are pending requests from CAN, and sends a heartbeat message.
		 * The heartbeat message encodes some data in the first 4 bytes of the CAN message, such as
		 * the number of errors and the version of scandal */
		handle_scandal();
		//UART_printf("Adminh = %d\n", scandal_get_in_channel_value(5));
		sc_time_t curr_time = sc_get_timer();

		if (curr_time > one_sec_timer + 100) {
			//temp_seatbelt();
			one_sec_timer = sc_get_timer();
		}


		// Toggle indicators when enabled
		// not the most efficient implementation but im tired
		if(curr_time > (indicator_timer + INDICATOR_PERIOD)){
			if (hazard_state){
				UART_printf("Hazard lights toggled...\n");
				GPIO_ToggleValue(LEFT_INDICATOR_PORT, LEFT_INDICATOR_PIN);
				GPIO_ToggleValue(RIGHT_INDICATOR_PORT, RIGHT_INDICATOR_PIN);
			}
			else if(indicator_right_en){
				UART_printf("Indicator right toggle\n\r");
				GPIO_ToggleValue(RIGHT_INDICATOR_PORT, RIGHT_INDICATOR_PIN);
			}
			else if(indicator_left_en){
				UART_printf("Indicator left toggle\n\r");
				GPIO_ToggleValue(LEFT_INDICATOR_PORT, LEFT_INDICATOR_PIN);
			}
			else {
				GPIO_SetValue(LEFT_INDICATOR_PORT, LEFT_INDICATOR_PIN, 0);
				GPIO_SetValue(RIGHT_INDICATOR_PORT, RIGHT_INDICATOR_PIN, 0);
			}

			indicator_timer = sc_get_timer();
		}
		//UART_printf("AdMinh2 = %d\n",scandal_get_in_channel_value(4));
		int speed_val=scandal_get_in_channel_value(4);
		speed_val=speed_val/1000;
		SHIFT_SPEED(speed_val);
		SHIFT_CRUISE(0);
		CHECK_BATTERY();
		//scandal_delay(1000);//1s
		//setMatch_timer32PWM (1, 2, 310000);
		//scandal_delay(1000);//1s
		//setMatch_timer32PWM (1, 2, 500000);
		//setMatch_timer32PWM (1, 2, 310000);
		/* Tickle the watchdog so we don't reset */
		WDT_Feed();
	}
}

void init_LEDS(void)
{
	GPIO_SetDir(RIGHT_INDICATOR_PORT, RIGHT_INDICATOR_PIN, 1);
	GPIO_SetDir(LEFT_INDICATOR_PORT, LEFT_INDICATOR_PIN, 1);
	GPIO_SetDir(BACKLIGHT_ONE_PORT, BACKLIGHT_ONE_PIN, 1);

	GPIO_SetDir(PARK_LED_PORT, PARK_LED_PIN, 1);
	GPIO_SetDir(REVERSE_LED_PORT, REVERSE_LED_PIN, 1);
	GPIO_SetDir(NTRL_LED_PORT, NTRL_LED_PIN, 1);
	GPIO_SetDir(NTRL_PULLUP_LED_PORT, NTRL_PULLUP_LED_PIN, 1);
	GPIO_SetDir(DRIVE_LED_PORT, DRIVE_LED_PIN, 1);
	GPIO_SetDir(DRIVE_PULLUP_LED_PORT, DRIVE_PULLUP_LED_PIN, 1);
	GPIO_SetDir(ECO_LED_PORT, ECO_LED_PIN, 1);
	GPIO_SetDir(CRUISE_TOGGLE_LED_PORT, CRUISE_TOGGLE_LED_PIN, 1);

	GPIO_SetDir(HANDBRAKE_LED_PORT, HANDBRAKE_LED_PIN, 1);
	GPIO_SetDir(CHECK_LED_PORT, CHECK_LED_PIN, 1);
	GPIO_SetDir(BRAKE_FAIL_PORT, BRAKE_FAIL_PIN, 1);
	GPIO_SetDir(HEADLIGHT_FAILURE_LED_PORT, HEADLIGHT_FAILURE_LED_PIN, 1);
	GPIO_SetDir(CTRL_LED_PORT, CTRL_LED_PIN, 1);
	GPIO_SetDir(WARNING_LED_PORT, WARNING_LED_PIN, 1);
	GPIO_SetDir(LOWBATT_PORT, LOWBATT_PIN, 1);
	GPIO_SetDir(POS_LED_PORT, POS_LED_PIN, 1);
	GPIO_SetDir(LOWBEAM_PORT, LOWBEAM_PIN, 1);
	GPIO_SetDir(HIGHBEAM_PORT, HIGHBEAM_PIN, 1);
	GPIO_SetDir(SEATBELT_LED_PORT, SEATBELT_LED_PIN, 1);
}

void init_ShiftRegisters(void)
{
	GPIO_SetDir(FUEL_DATA_PORT, FUEL_DATA_PIN, 1);
	GPIO_SetDir(FUEL_RCLK_PORT, FUEL_RCLK_PIN, 1);
	GPIO_SetDir(FUEL_SRCLK_PORT, FUEL_SRCLK_PIN, 1);

	GPIO_SetDir(SPEED_DATA_PORT, SPEED_DATA_PIN, 1);
	GPIO_SetDir(SPEED_RCLK_PORT, SPEED_RCLK_PIN, 1);
	GPIO_SetDir(SPEED_SRCLK_PORT, SPEED_SRCLK_PIN, 1);

	GPIO_SetDir(CRUISE_DATA_PORT, CRUISE_DATA_PIN, 1);
	GPIO_SetDir(CRUISE_RCLK_PORT, CRUISE_RCLK_PIN, 1);
	GPIO_SetDir(CRUISE_SRCLK_PORT, CRUISE_SRCLK_PIN, 1);
}

void debug_LEDS(void)
{
	GPIO_SetValue(RIGHT_INDICATOR_PORT, RIGHT_INDICATOR_PIN, 0);
	GPIO_SetValue(LEFT_INDICATOR_PORT, LEFT_INDICATOR_PIN, 0);
	GPIO_SetValue(BACKLIGHT_ONE_PORT, BACKLIGHT_ONE_PIN, 0);

	GPIO_SetValue(PARK_LED_PORT, PARK_LED_PIN, 0);
	GPIO_SetValue(REVERSE_LED_PORT, REVERSE_LED_PIN, 0);
	GPIO_SetValue(NTRL_LED_PORT, NTRL_LED_PIN, 0);
	GPIO_SetValue(NTRL_PULLUP_LED_PORT, NTRL_PULLUP_LED_PIN, 0);
	GPIO_SetValue(DRIVE_LED_PORT, DRIVE_LED_PIN, 0);
	GPIO_SetValue(DRIVE_PULLUP_LED_PORT, DRIVE_PULLUP_LED_PIN, 0);
	GPIO_SetValue(ECO_LED_PORT, ECO_LED_PIN, 0);
	GPIO_SetValue(CRUISE_TOGGLE_LED_PORT, CRUISE_TOGGLE_LED_PIN, 0);

	GPIO_SetValue(HANDBRAKE_LED_PORT, HANDBRAKE_LED_PIN, 0);
	GPIO_SetValue(CHECK_LED_PORT, CHECK_LED_PIN, 0);
	GPIO_SetValue(BRAKE_FAIL_PORT, BRAKE_FAIL_PIN, 0);
	GPIO_SetValue(HEADLIGHT_FAILURE_LED_PORT, HEADLIGHT_FAILURE_LED_PIN, 0);
	GPIO_SetValue(CTRL_LED_PORT, CTRL_LED_PIN, 0);
	GPIO_SetValue(WARNING_LED_PORT, WARNING_LED_PIN, 0);
	GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	GPIO_SetValue(POS_LED_PORT, POS_LED_PIN, 0);
	GPIO_SetValue(LOWBEAM_PORT, LOWBEAM_PIN, 0);
	GPIO_SetValue(HIGHBEAM_PORT, HIGHBEAM_PIN, 0);
	GPIO_SetValue(SEATBELT_LED_PORT, SEATBELT_LED_PIN, 0);
}

void LEDS_on(void)
{
	GPIO_SetValue(3, 1, 1);
}

void temp_seatbelt(void)
{
	if (GPIO_GetValue(SEATBELT_LED_PORT, SEATBELT_LED_PIN))
	{
		GPIO_SetValue(SEATBELT_LED_PORT, SEATBELT_LED_PIN, 1);
	}
	else
	{
		GPIO_SetValue(SEATBELT_LED_PORT, SEATBELT_LED_PIN, 0);
	}
}

void clear_drive_leds(void)
{
	GPIO_SetValue(REVERSE_LED_PORT, REVERSE_LED_PIN, 0);
	GPIO_SetValue(PARK_LED_PORT, PARK_LED_PIN, 0);
	GPIO_SetValue(DRIVE_LED_PORT, DRIVE_LED_PIN, 0);
	GPIO_SetValue(NTRL_LED_PORT, NTRL_LED_PIN, 0);
	GPIO_SetValue(CRUISE_TOGGLE_LED_PORT, CRUISE_TOGGLE_LED_PIN, 0);
	GPIO_SetValue(ECO_LED_PORT, ECO_LED_PIN, 0);
	GPIO_SetValue(NTRL_PULLUP_LED_PORT, NTRL_PULLUP_LED_PIN, 0);
	GPIO_SetValue(DRIVE_PULLUP_LED_PORT, DRIVE_PULLUP_LED_PIN, 0);
}

void toggle_backlights(int on_off)
{
	// Quick and dirty solution
	GPIO_SetValue(BACKLIGHT_ONE_PORT, BACKLIGHT_ONE_PIN, on_off);
}

// Simple function to send serial data to one or more shift registers by iterating backwards through an array.
// Although g_registers exists, they may not all be being used, hence the input parameter.
void sendSerialData_SPEED (int registerCount, int *pValueArray)
{
    int reg,bitMask;
    // Signal to the 595s to listen for data
    GPIO_SetValue( SPEED_RCLK_PORT, SPEED_RCLK_PIN, 0);
    for (reg = registerCount; reg > 0; reg--)
    {
    	int value = pValueArray [reg - 1];
        for ( bitMask = 128; bitMask > 0; bitMask >>= 1)
        {
        	GPIO_SetValue( SPEED_SRCLK_PORT, SPEED_SRCLK_PIN, 0);
           	GPIO_SetValue(SPEED_DATA_PORT, SPEED_DATA_PIN, value & bitMask ? 0 : 1);//Data
           	GPIO_SetValue(SPEED_SRCLK_PORT, SPEED_SRCLK_PIN, 1);
        }
    }
    // Signal to the 595s that I'm done sending
    GPIO_SetValue(SPEED_RCLK_PORT, SPEED_RCLK_PIN, 1);
}

void SHIFT_SPEED(int num)
{
	int g_digits [10];

    // Current number being displayed
    int g_numberToDisplay = num;

    // Number of shift registers in use
    const int g_registers = 4;

    // Array of numbers to pass to shift registers
    int g_registerArray [g_registers];
	  // Setup the digits array
      // a = 8 b = 4 c = 2 d = 64 e = 32 f = 1 g = 16
      g_digits [0] = 8 + 4 + 2 + 64 + 32 + 1;
      g_digits [1] = 4 + 2;
      g_digits [2] = 8 + 4 + 16 + 32 + 64;
      g_digits [3] = 8 + 4 + 16 + 2 + 64;
      g_digits [4] = 1 + 16 + 4 + 2;
      g_digits [5] = 8 + 1 + 16 + 2 + 64;
      g_digits [6] = 8 + 1 + 16 + 2 + 64 + 32;
      g_digits [7] = 8 + 4 + 2;
      g_digits [8] = 8 + 4 + 2 + 64 + 32 + 1 + 16;
      g_digits [9] = 8 + 4 + 2 + 1 + 16 + 64;

  if (g_numberToDisplay < 10)
  {
  	g_registerArray [2] = 0;
   	g_registerArray [1] = 0;
   	g_registerArray [0] = g_digits [g_numberToDisplay];
  }

  else if (g_numberToDisplay < 100)
  {
    g_registerArray [2] = 0; //Oh yes saving power
    g_registerArray [1] = g_digits [g_numberToDisplay / 10];
    g_registerArray [0] = g_digits [g_numberToDisplay % 10];
  }

  else if (g_numberToDisplay < 1000)
  {
    g_registerArray [2] = g_digits [g_numberToDisplay / 100];
    g_registerArray [1] = g_digits [(g_numberToDisplay % 100) / 10];
    g_registerArray [0] = g_digits [g_numberToDisplay % 10];
  }
	sendSerialData_SPEED (g_registers, g_registerArray);
}

void sendSerialData_CRUISE (int registerCount, int *pValueArray)
{
	int reg,bitMask;
    // Signal to the 595s to listen for data
   	GPIO_SetValue( CRUISE_RCLK_PORT, CRUISE_RCLK_PIN, 0);
    for (reg = registerCount; reg > 0; reg--)
    {
    	int value = pValueArray [reg - 1];
        for ( bitMask = 128; bitMask > 0; bitMask >>= 1)
        {
        	GPIO_SetValue( CRUISE_SRCLK_PORT, CRUISE_SRCLK_PIN, 0);
        	GPIO_SetValue(CRUISE_DATA_PORT, CRUISE_DATA_PIN, value & bitMask ? 0 : 1);//Data
        	GPIO_SetValue(CRUISE_SRCLK_PORT, CRUISE_SRCLK_PIN, 1);
        }
      }
      // Signal to the 595s that I'm done sending
      GPIO_SetValue(CRUISE_RCLK_PORT, CRUISE_RCLK_PIN, 1);
}  // sendSerialData

void SHIFT_CRUISE(int num)
{
    int g_digits [10];
    // Current number being displayed
    int g_numberToDisplay = num;
    // Number of shift registers in use
    const int g_registers = 4;

    // Array of numbers to pass to shift registers
    int g_registerArray [g_registers];
	      // Setup the digits array
      // a = 8 b = 4 c = 2 d = 64 e = 32 f = 1 g = 16
      g_digits [0] = 8 + 4 + 2 + 64 + 32 + 1;
      g_digits [1] = 4 + 2;
      g_digits [2] = 8 + 4 + 16 + 32 + 64;
      g_digits [3] = 8 + 4 + 16 + 2 + 64;
      g_digits [4] = 1 + 16 + 4 + 2;
      g_digits [5] = 8 + 1 + 16 + 2 + 64;
      g_digits [6] = 8 + 1 + 16 + 2 + 64 + 32;
      g_digits [7] = 8 + 4 + 2;
      g_digits [8] = 8 + 4 + 2 + 64 + 32 + 1 + 16;
      g_digits [9] = 8 + 4 + 2 + 1 + 16 + 64;

      if (g_numberToDisplay < 10)
      {
        g_registerArray [2] = 0;
        g_registerArray [1] = 0;
        g_registerArray [0] = g_digits [g_numberToDisplay];
      }

  	else if (g_numberToDisplay < 100)
  	{
    	g_registerArray [2] = 0;
    	g_registerArray [1] = g_digits [g_numberToDisplay / 10];
    	g_registerArray [0] = g_digits [g_numberToDisplay % 10];
  	}

    else if (g_numberToDisplay < 1000)
  	{
    	g_registerArray [2] = g_digits [g_numberToDisplay / 100];
    	g_registerArray [1] = g_digits [(g_numberToDisplay % 100) / 10];
    	g_registerArray [0] = g_digits [g_numberToDisplay % 10];
  	}

	sendSerialData_CRUISE (g_registers, g_registerArray);
}


//Fuel
void sendSerialData_FUEL (int registerCount, int *pValueArray)
{
	int reg,bitMask;
    // Signal to the 595s to listen for data
   	GPIO_SetValue( FUEL_RCLK_PORT, FUEL_RCLK_PIN, 0);
    for (reg = registerCount; reg > 0; reg--)
    {
    	int value = pValueArray [reg - 1];
        for ( bitMask = 128; bitMask > 0; bitMask >>= 1)
        {
        	GPIO_SetValue( FUEL_SRCLK_PORT, FUEL_SRCLK_PIN, 0);
           	GPIO_SetValue(FUEL_DATA_PORT, FUEL_DATA_PIN, value & bitMask ? 0 : 1);//Data
        	GPIO_SetValue(FUEL_SRCLK_PORT, FUEL_SRCLK_PIN, 1);
        }
    }
    // Signal to the 595s that I'm done sending
    GPIO_SetValue(FUEL_RCLK_PORT, FUEL_RCLK_PIN, 1);
}  // sendSerialData

void SHIFT_FUEL(int num)
{
    int g_digits [10];

    // Current number being displayed
    int g_numberToDisplay = num;

    // Number of shift registers in use
    const int g_registers = 4;

    // Array of numbers to pass to shift registers
    int g_registerArray [g_registers];
	  // Setup the digits array
      // a = 8 b = 4 c = 2 d = 64 e = 32 f = 1 g = 16
      g_digits [0] = 0; //Nothing turns on
      g_digits [1] = 1; // 1 fuel led turns on, only turn on QA
      g_digits [2] = 1 + 2; // Turn on QA and QB
      g_digits [3] = 1 + 2 + 4; // Turn on QA, QB, QC
      g_digits [4] = 1 + 2 + 4 + 8;
      g_digits [5] = 1 + 2 + 4 + 8 + 16;
      g_digits [6] = 1 + 2 + 4 + 8 + 16 + 32;
      g_digits [7] = 1 + 2 + 4 + 8 + 16 + 32 + 64;
      g_digits [8] = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 128;
      //g_digits [9] = 8 + 4 + 2 + 1 + 16 + 64;

    if (g_numberToDisplay < 9)
    {
    	g_registerArray [1] = g_digits [0];
        g_registerArray [0] = g_digits [g_numberToDisplay];
    }
  	else if (g_numberToDisplay == 9)
  	{
    	g_registerArray [1] = g_digits [1];
    	g_registerArray [0] = g_digits [8];
  	}
  	else if (g_numberToDisplay == 10)
  	{
    	g_registerArray [1] = g_digits [2];
    	g_registerArray [0] = g_digits [8];
  	}
  	sendSerialData_FUEL (g_registers, g_registerArray);
}

void CHECK_BATTERY(void)
{
	int battery_value = scandal_get_in_channel_value(5);

	if (battery_value <= 92000)
	{
		SHIFT_FUEL(0);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 1);
		//scandal_delay(500);
		//GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
		//scandal_delay(500);
	}
	else if (battery_value <= 96200 && battery_value > 91000)
	{
		SHIFT_FUEL(1);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 1);
	}
	else if (battery_value <= 102400 && battery_value > 96200)
	{
		SHIFT_FUEL(2);
	}
	else if (battery_value <= 108600 && battery_value > 102400)
	{
		SHIFT_FUEL(3);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value <= 114800 && battery_value > 108600)
	{
		SHIFT_FUEL(4);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value <= 121000 && battery_value > 114800)
	{
		SHIFT_FUEL(5);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value <= 127200 && battery_value > 121000)
	{
		SHIFT_FUEL(6);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value <= 133400 && battery_value > 127200)
	{
		SHIFT_FUEL(7);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value <= 139600 && battery_value > 133400)
	{
		SHIFT_FUEL(8);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value <= 145800 && battery_value > 139600)
	{
		SHIFT_FUEL(9);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
	else if (battery_value > 145800)
	{
		SHIFT_FUEL(10);
		GPIO_SetValue(LOWBATT_PORT, LOWBATT_PIN, 0);
	}
}
