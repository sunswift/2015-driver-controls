#define RED_LED_PORT            2   // Port for led
#define RED_LED_BIT             8	// Bit on port for led
#define YELLOW_LED_PORT         2	// Port for led
#define YELLOW_LED_BIT          7	// Bit on port for led



// PORTS

#define PARKINGMODE_PORT            2
#define PARKINGMODE_PIN             9


#define REVERSEMODE_PORT            0
#define REVERSEMODE_PIN             6

#define NEUTRALMODE_PORT            2
#define NEUTRALMODE_PIN             8

#define DRIVEMODE_PORT              1
#define DRIVEMODE_PIN               8

#define ECOMODE_PORT                1
#define ECOMODE_PIN                 5

// PINS

#define START_PORT                  0
#define START_PIN                   2

// CRUISE PINS
#define CRUISE_TOGGLE_PORT          0
#define CRUISE_TOGGLE_PIN           2

#define CRUISE_INCREMENT_PORT       1
#define CRUISE_INCREMENT_PIN        9

#define CRUISE_DECREMENT_PORT       2
#define CRUISE_DECREMENT_PIN        5

#define START_PERIOD                2000



#define LED_TOGGLE_TICKS        100 // 100 ticks = 1 Hz flash rate
#define FAST_LED_TOGGLE_TICKS   25  // 100 ticks = 1 Hz flash rate
#define COUNT_MAX		        3   // how high to count on the LED display

// Wavesculptor CAN addresses
#define LEFT_WS_ADDRESS_BASE    0x400
#define RIGHT_WS_ADDRESS_BASE   0x420
#define LEFT_WS_ADDRESS_CTRL    0x500
#define RIGHT_WS_ADDRESS_CTRL   0x520
#define DRIVE_OFFSET            0x01
#define POWER_OFFSET            0x02

// Wavesculptor Constants
#define RPM_REV_MAX             -1500
#define RPM_FWD_MAX             20000

// Car Gearing States
#define PARKED                  0
#define REVERSE                 1
#define NEUTRAL                 2
#define ECODRIVE                3
#define DRIVE                   4

// Precharge State
#define DISCHARGED              0
#define CHARGED                 1

// General flags
#define DISABLED                0
#define ENABLED                 1

// ADC configuration
#define ACCELERATOR_ADC_CHANNEL 0
#define BRAKE_ADC_CHANNEL       1

// GPIO configuration
#define REVERSE_PIN             1
#define BRAKE_FAULT_PIN         2
#define ACCELERATOR_FAULT_PIN   3

// Accelerator range adjustment
#define ACCELERATOR_LOW_VALUE   412.0      // ADC output when pedel is 0
#define ACCELERATOR_HIGH_VALUE  650.0    // ADC output when pedal maxed out
#define ACCELERATOR_DEADZONE    20.0      // Deadzone in ADC values
#define ACCEL_SCALE             1        // Downscaling factor to limit max val

// Brake range adjustment
#define BRAKE_LOW_VALUE         335             // ADC output when pedel is 0
#define BRAKE_HIGH_VALUE        550.0          // ADC output when pedal maxed out
#define BRAKE_DEADZONE          30.0             // Deadzone in ADC values
#define BRAKE_SCALE             2                 // Downscaling factor to limit max val

#define REV_PORT                2
#define REV_PIN                 5

#define SPD_LIMIT_PORT          3
#define SPD_LIMIT_PIN           1

#define WHEEL_DIAMETER          0.56
#define PI                      3.14

#define CRUISE_SPEED_DEFAULT    70



/* Drive State Struct - Holds all info on current drive state of vehicle */
typedef struct drive_state {
    uint8_t     drive_mode;
    uint8_t     regen;
    uint8_t     precharge;
    uint8_t     cruise;
    uint32_t    cruise_speed;
    float       accel_ratio;
    float       brake_ratio;
} drive_state;


#define WS_REDUCED_NUM_MEMBERS    11
/* Reduced struct of output wavesculptor data, this should contain
 * everything which needs to be sent to the telemetry network */
typedef struct ws_small_struct {
    uint32_t    active_motor;
    uint32_t    motor_velocity;
    uint32_t    errors;
    uint32_t    limits;
    uint32_t    bus_current;
    uint32_t    bus_voltage;
    uint32_t    phase_a;
    uint32_t    phase_b;
    uint32_t    motor_temp;
    uint32_t    heatsink_temp;
    uint32_t    odometer;
} ws_small_struct;



// Command mapping for communication with other CAN nodes (Davina)
#define CHANNEL_CONTROLS_DRIVE           1

#define OFFSET_ONOFF                   0x1
/*
 //Anmol's Code
#define OFFSET_PARKINGMODE             0x2
#define OFFSET_NEUTRALMODE             0x4
#define OFFSET_REVERSEMODE             0x8
#define OFFSET_DRIVEMODE               0x10
#define OFFSET_ECOMODE                 0x20

#define OFFSET_CRUISE                  0x40

*/


#define OFFSET_BRAKING                 0x200000 //Brake Lights for SUTI, its fine to keep this the same as cruise toggle.


#define OFFSET_PARKINGMODE               0x8
#define OFFSET_REVERSEMODE              0x10
#define OFFSET_NEUTRALMODE              0x20
#define OFFSET_DRIVEMODE                0x40
#define OFFSET_ECOMODE                  0x80
#define OFFSET_CRUISE                   0x100
