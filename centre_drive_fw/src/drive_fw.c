/* --------------------------------------------------------------------------
    Drive Node Firmware (dyno day)
    File name: drive_fw.c
    Author: Anmol Thind, Tom Dransfield
    Description: The center console - wavesculptor interface firmware, built only
                 for Dyno Day. Feature Incomplete.

    Copyright (C) Anmol Thind, Tom Dransfield. 2015

    Created: 05/04/2015
   -------------------------------------------------------------------------- */


#include <scandal/engine.h>
#include <scandal/message.h>
#include <scandal/leds.h>
#include <scandal/utils.h>
#include <scandal/uart.h>
#include <scandal/stdio.h>
#include <scandal/wdt.h>
#include <scandal/wavesculptor.h>
#include <scandal/error.h>

#include <string.h>

#include <project/driver_config.h>
#include <project/target_config.h>
#include <project/mlink_master.h>
#include <arch/can.h>
#include <arch/uart.h>
#include <arch/timer.h>
#include <arch/adc.h>
#include <arch/gpio.h>
#include <arch/types.h>
#include <arch/i2c.h>


void setup(void);
void poll_gear_state();
void send_drive_commands(void);
void split_ws_drive_command(float rpm, float motor_current_ratio);
void split_ws_power_command(float bus_current_ratio);
void split_ws_cruise_command(uint32_t cruise_speed);
void reduce_ws_data(void);
float get_accelerator_value();
float get_brake_value();
float get_ADC(uint32_t channelNum);

void send_ws_dummy_velocity(void);
void send_cruise_signal(void);

void check_speed_limit(void);

void handler_cruise(void);
void handler_cruise_up(void);
void handler_cruise_down(void);

void drive_setup_handlers(void);


drive_state eve;
uint32_t state2 = 0x00000000; // Stores peripheral states as flags (Davina Wed/Jun/15)


Wavesculptor_Output_Struct  Left_WS;
Wavesculptor_Output_Struct  Right_WS;

ws_small_struct     left_ws_small;
ws_small_struct     right_ws_small;

sc_time_t   brake_timer_init;
sc_time_t   brake_timer_end;
sc_time_t   brake_test_time;


// Handles Wavesculptor CAN packets
void WS22_handler(can_msg *msg){
    //UART_printf("IN WS CAN HANDLER\n");
    uint16_t baseAddress = msg->id & 0x7E0;

    if(baseAddress == Left_WS.BaseAddress){
            //UART_printf("LEFT WS CAN HANDLER\n");
            scandal_store_ws_message(msg, &Left_WS);
    } else if(baseAddress == Right_WS.BaseAddress){
            //UART_printf("RIGHT WS CAN HANDLER\n");
            scandal_store_ws_message(msg, &Right_WS);
    } else{
        UART_printf("Address of CAN packet did not match any known address. ID = 0x%x\r\n", (unsigned int) msg->id);
    }
}



void setup(void) {

    //Looks like ADC requires GPIO initialisation.
    GPIO_Init();
    GPIO_SetDir(REV_PORT, REV_PIN, 0);
    GPIO_SetDir(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);
    GPIO_SetValue(SPD_LIMIT_PORT, SPD_LIMIT_PIN, 0);
    GPIO_SetDir(START_PORT, START_PIN, 0);
    init_pins();

    //MLINK initialisation
    mlink_setup();

    eve.drive_mode = PARKED;
    eve.regen = ENABLED;
    eve.precharge = DISCHARGED;
    eve.cruise = DISABLED;
    eve.cruise_speed = CRUISE_SPEED_DEFAULT;
    eve.accel_ratio = 0;
    eve.brake_ratio = 0;

    Left_WS.BaseAddress = LEFT_WS_ADDRESS_BASE;
    Right_WS.BaseAddress = RIGHT_WS_ADDRESS_BASE;

    Left_WS.ControlAddress = LEFT_WS_ADDRESS_CTRL;
    Right_WS.ControlAddress = RIGHT_WS_ADDRESS_CTRL;

    register_standard_message_handler(WS22_handler);

    /* Initialise the watchdog timer. If the node crashes, it will restart automatically */
    WDT_Init();

    /* Initialise Scandal, registers for the config messages, timesync messages and in channels */
    scandal_init();

    //Charith's Hack - What does this even do, call Charith...
    can_register_id(0x7FF, 0x403, 0, 0);
    can_register_id(0x7FF, 0x423, 0, 0);

    //Charith's Hack.
    while(can_register_id(0, 0, 0, 0)==NO_ERR)
    ;


    /* Initialise UART0 for USB debugging */
    UART_Init(115200);

    /* Wait until UART is ready */
    scandal_delay(1000);

    //Do our ADC setup here, ALSO NEED TO DEFINE PULLUPS IN driver_config.h
    ADC_Init(ADC_MAXCLK);
    // This should be the accelerator channel
    ADC_EnableChannel(0);
    // This should be the brake channel
    ADC_EnableChannel(1);

    drive_setup_handlers();

	/* Display welcome header over UART */
    UART_printf("Welcome to the jungle \n\r");
    UART_printf("Stuff should be happening \n\r");
}


void send_drive_commands(void) {

    if (eve.precharge == DISCHARGED) return;

    switch(eve.drive_mode) {
        case PARKED:
            split_ws_drive_command(0, 0);
            if (eve.brake_ratio <= (0.05 / BRAKE_SCALE)) {
                //UART_printf("Not braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_BRAKING);
            }
            else {
                //UART_printf("Braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_ONOFF | OFFSET_BRAKING);
            }
            break;

        case REVERSE:
            UART_printf("Sending REVERSE commands...");
            if (eve.brake_ratio <= (0.05 / BRAKE_SCALE)) {
                split_ws_drive_command(RPM_REV_MAX, eve.accel_ratio);
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_BRAKING);
                //UART_printf("Not braking\n");
            }
            else {
                split_ws_drive_command(0, eve.brake_ratio);
                //UART_printf("braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_ONOFF | OFFSET_BRAKING);

            }
            break;

        case NEUTRAL:
            split_ws_drive_command(0, 0);
            if (eve.brake_ratio <= (0.05 / BRAKE_SCALE)) {
                //UART_printf("Not braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_BRAKING);
            }
            else {
                //UART_printf("Braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_ONOFF | OFFSET_BRAKING);
            }
            break;

        case ECODRIVE:
            split_ws_power_command(1);
            if (eve.brake_ratio <= (0.05 / BRAKE_SCALE)) {
                //UART_printf("Not braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_BRAKING);
                split_ws_drive_command(RPM_FWD_MAX, eve.accel_ratio);
            }
            else {
                //UART_printf("Braking\n");
                //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_ONOFF | OFFSET_BRAKING);
                split_ws_drive_command(0, eve.brake_ratio);
            }
            break;

        case DRIVE:
            UART_printf("Sending DRIVE commands...");
            split_ws_power_command(1);

            if(eve.cruise = DISABLED) {
                if (eve.brake_ratio <= (0.05 / BRAKE_SCALE)) {
                    //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_BRAKING);
                    UART_printf("Not braking\n");

                    split_ws_drive_command(RPM_FWD_MAX, eve.accel_ratio);
                }
                else {
                    //scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_ONOFF | OFFSET_BRAKING);
                    UART_printf("braking\n");
                    split_ws_drive_command(0, eve.brake_ratio);
                }
            }
            else {
                split_ws_cruise_command(eve.cruise);
            }

    }

}

void send_ws_dummy_velocity() {
    UART_printf("Send cruise velocity\n");
    scandal_send_ws_drive_command(LEFT_WS_ADDRESS_CTRL + DRIVE_OFFSET, 408, 0.2);
    scandal_send_ws_drive_command(RIGHT_WS_ADDRESS_CTRL + DRIVE_OFFSET, 408, 0.2);
}

void send_cruise_signal() {
    UART_printf("Send cruise\n");
    scandal_send_ws_drive_command(LEFT_WS_ADDRESS_CTRL + DRIVE_OFFSET, 0, 0);
    scandal_send_ws_drive_command(RIGHT_WS_ADDRESS_CTRL + DRIVE_OFFSET, 0, 0);
}


/* Simple functions to split the same commands to both controllers, note
 * we can only use it for dyno day. We probably need to implement the variable
 * motor torque control like old STEVE does eventually */

void split_ws_drive_command(float rpm, float motor_current_ratio) {
    scandal_send_ws_drive_command(LEFT_WS_ADDRESS_CTRL + DRIVE_OFFSET, rpm, motor_current_ratio);
    scandal_send_ws_drive_command(RIGHT_WS_ADDRESS_CTRL + DRIVE_OFFSET, rpm, motor_current_ratio);
}

void split_ws_power_command(float bus_current_ratio) {
    scandal_send_ws_drive_command(LEFT_WS_ADDRESS_CTRL + POWER_OFFSET, 0, bus_current_ratio);
    scandal_send_ws_drive_command(RIGHT_WS_ADDRESS_CTRL + POWER_OFFSET, 0, bus_current_ratio);
}

void split_ws_cruise_command(uint32_t cruise_speed) {
    float rpm = (cruise_speed * 1000) / (60 * PI * WHEEL_DIAMETER);
    scandal_send_channel(LEFT_WS_ADDRESS_CTRL + DRIVE_OFFSET, rpm, 1);
    scandal_send_channel(RIGHT_WS_ADDRESS_CTRL + DRIVE_OFFSET, rpm, 1);
}


void reduce_ws_data(void) {
    left_ws_small.active_motor = Left_WS.ActiveMotor;
    left_ws_small.motor_velocity = Left_WS.MotorVelocity;
    left_ws_small.errors = Left_WS.ErrorFlags;
    left_ws_small.limits = Left_WS.LimitFlags;
    left_ws_small.bus_current = Left_WS.BusCurrent;
    left_ws_small.bus_voltage = Left_WS.BusVoltage;
    left_ws_small.phase_a = Left_WS.Phase_1;
    left_ws_small.phase_b = Left_WS.Phase_2;
    left_ws_small.motor_temp = Left_WS.MotorTemp;
    left_ws_small.heatsink_temp = Left_WS.HeatsinkTemp;
    left_ws_small.odometer = Left_WS.Odometer;

    right_ws_small.active_motor = Right_WS.ActiveMotor;
    right_ws_small.motor_velocity = Right_WS.MotorVelocity;
    right_ws_small.errors = Right_WS.ErrorFlags;
    right_ws_small.limits = Right_WS.LimitFlags;
    right_ws_small.bus_current = Right_WS.BusCurrent;
    right_ws_small.bus_voltage = Right_WS.BusVoltage;
    right_ws_small.phase_a = Right_WS.Phase_1;
    right_ws_small.phase_b = Right_WS.Phase_2;
    right_ws_small.motor_temp = Right_WS.MotorTemp;
    right_ws_small.heatsink_temp = Right_WS.HeatsinkTemp;
    right_ws_small.odometer = Right_WS.Odometer;

}

/* Gets the accelerator value as a float between
 * 0 and 1. Is not a direct mapping of the ADC value,
 * includes a deadzone in the physical movement of the pedal.
 * TODO: Verify signal using pedal digital outputs to ensure signal is correct
 */
float get_accelerator_value(){
    float accel_val = 0.0;

    // Polling Method
    float adc_output = get_ADC(0);

    //UART_printf("ACCEL ADC: %f    ", adc_output);
    // In case we use weird interrupt method
    //uint32_t adc_output = get_ADC(ACCELERATOR_ADC_CHANNEL);

    // Find the accelerator value taking into account deadzones due to the output
    // range of the brake pedal. Also includes the intended input deadzone.
    accel_val = (adc_output - ACCELERATOR_LOW_VALUE - ACCELERATOR_DEADZONE);
    accel_val /= (ACCELERATOR_HIGH_VALUE - ACCELERATOR_LOW_VALUE - ACCELERATOR_DEADZONE);
    //if(accel_val - eve.accel_ratio > 0.5) accel_val = eve.accel_ratio + 0.1;
    //if(eve.accel_ratio - accel_val > 0.5) accel_val = eve.accel_ratio - 0.1;
    if(accel_val > 1) accel_val = 1;
    else if(accel_val < 0.0) accel_val = 0.0;
    accel_val = accel_val / ACCEL_SCALE;
    return accel_val;
}

float get_brake_value(){
    float brake_val = 0.0;

    // Polling Method
    float adc_output = get_ADC(1);
    //UART_printf("BRAKE ADC: %f\n", adc_output);
    // In case we use weird interrupt method
    //uint32_t adc_output = get_ADC(ACCELERATOR_ADC_CHANNEL);

    brake_val = (adc_output - BRAKE_LOW_VALUE - BRAKE_DEADZONE);
    brake_val /= (BRAKE_HIGH_VALUE - BRAKE_LOW_VALUE - BRAKE_DEADZONE);
    //if(brake_val - eve.brake_ratio > 0.5) brake_val = eve.brake_ratio + 0.1;
    //if(eve.brake_ratio - brake_val > 0.5) brake_val = eve.brake_ratio - 0.1;
    if (brake_val > 1) brake_val = 1;
    else if(brake_val < 0.0) brake_val = 0.0;
    brake_val = brake_val / BRAKE_SCALE;
    return brake_val;
}


// THIS IS ONLY HERE IN CASE SIMPLE POLLING DOESN'T WORK
float get_ADC(uint32_t channelNum) {
    uint32_t i = 0;
    ADC_Read(channelNum);
    while((!ADCIntDone) && (i<10)) {
        i++;
    }
    //UART_printf("ADC_I:%d\n\r",i);
    scandal_delay(1);

    if(i>=10) {
        return ACCELERATOR_HIGH_VALUE;
    } else {
        return ADCValue[channelNum];
    }
}

void poll_gear_state(void) {

    uint8_t prev_drive_mode = eve.drive_mode;

    if (GPIO_GetValue(PARKINGMODE_PORT, PARKINGMODE_PIN)) {
        eve.drive_mode = PARKED;
        //UART_printf("Parked mode\n");
    }
    else if (GPIO_GetValue(NEUTRALMODE_PORT, NEUTRALMODE_PIN)) {
        eve.drive_mode = NEUTRAL;
        //UART_printf("Neutral mode\n");
    }
    else if (GPIO_GetValue(REVERSEMODE_PORT, REVERSEMODE_PIN)) {
        eve.drive_mode = REVERSE;
        //UART_printf("Reverse mode\n");
    }
    else if (GPIO_GetValue(DRIVEMODE_PORT, DRIVEMODE_PIN)) {
        eve.drive_mode = DRIVE;
        //UART_printf("Drive mode\n");
    }
    else if (GPIO_GetValue(ECOMODE_PORT, ECOMODE_PIN)) {
        eve.drive_mode = ECODRIVE;
        //UART_printf("ECO mode\n");
    }

    if (eve.drive_mode != prev_drive_mode) {
        switch(eve.drive_mode){
            case PARKED:
                UART_printf("Parked mode\n");
                scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_PARKINGMODE);
                break;

            case REVERSE:
                UART_printf("Reverse mode\n");
                scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_REVERSEMODE);
                break;

            case NEUTRAL:
                UART_printf("Neutral mode\n");
                scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_NEUTRALMODE);
                break;

            case ECODRIVE:
                UART_printf("Eco mode\n");
                scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_NEUTRALMODE);
                break;

            case DRIVE:
                UART_printf("Drive mode\n");
                scandal_send_channel(CONTROL_HIGH, CHANNEL_CONTROLS_DRIVE, OFFSET_DRIVEMODE);
                break;
        }
    }
}

void check_speed_limit(void) {
    float avg_rpm = (Left_WS.MotorVelocity + Right_WS.MotorVelocity)/2;
    float vehicle_speed = 0.18849 * WHEEL_DIAMETER * avg_rpm;
    float left_wheel_speed = Left_WS.MotorVelocity * 0.18849 * WHEEL_DIAMETER;
    float right_wheel_speed = Right_WS.MotorVelocity * 0.18849 * WHEEL_DIAMETER;


    //UART_printf("Vehicle Speed: %3.2f km/h\n", vehicle_speed);
    UART_printf("LEFT WHEEL: %3.2f km/h      ", left_wheel_speed);
    UART_printf("RIGHT WHEEL: %3.2f km/h \n", right_wheel_speed);

    if (vehicle_speed >= 37.0 && vehicle_speed <= 43.0) {
        GPIO_SetValue(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);
    }
    else if (vehicle_speed >= 46.0 && vehicle_speed <= 54.0){
        GPIO_SetValue(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);
    }
    else if (vehicle_speed >= 56.0 && vehicle_speed <= 64.0){
        brake_timer_end = sc_get_timer();
        GPIO_SetValue(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);
    }
    else {
        GPIO_SetValue(SPD_LIMIT_PORT, SPD_LIMIT_PIN, 0);
    }

    brake_test_time = brake_timer_end - brake_timer_init;

}


/* Main Function */
int main(void) {

    setup();
    sc_time_t drive_timer = sc_get_timer();
    sc_time_t telem_timer = sc_get_timer();
    sc_time_t led_timer = sc_get_timer();
    sc_time_t start_timer = sc_get_timer();

    int led_state = 0;
    int timing_button = 0;
    int not_started = 0;

    // DELAY FOR PRECHARGE, HOW LONG SHOULD THIS BE ?
    UART_printf("Precharge 10 second delay...\n\r");
    //scandal_delay(10000);
    UART_printf("Precharge delay complete...\n\r");

    /* TESTING OVERRIDE */
    eve.drive_mode = PARKED;
    eve.regen = ENABLED;
    eve.precharge = CHARGED;
    eve.cruise = DISABLED;

    uint32_t mlink_buffer = (int)(1);


    /* This is the main loop, go for ever! */
    while (1) {
        /* This checks whether there are pending requests from CAN, and sends a heartbeat message.
         * The heartbeat message encodes some data in the first 4 bytes of the CAN message, such as
         * the number of errors and the version of scandal */

        handle_scandal();

        if (GPIO_GetValue(START_PORT, START_PIN) && not_started) {
            //UART_printf("start button depressed\n");
            if (!timing_button) {
                UART_printf("Started timing start button...\n");
                timing_button = 1;
                start_timer = sc_get_timer();
                mlink_send(0x00, MLINK_ON_SIGNAL_ADDR, (void*) &mlink_buffer);
            }
            else {
                if ((sc_get_timer() > (start_timer + START_PERIOD)) && eve.drive_mode == PARKED) {
                    UART_printf("Sent start signal\n");
                    mlink_send(0x00, MLINK_START_SIGNAL_ADDR, (void*) &mlink_buffer);
                    not_started = 0;
                    timing_button = 0;
                }
            }
        }
        else {
            //UART_printf("start button not depressed\n");
            timing_button = 0;
        }


        // Send drive commands at a rate of 20 Hz
        if (sc_get_timer() > drive_timer + 50) {
            //poll_gear_state();

            //check_speed_limit();
            //UART_printf("Brake test time (100 -> 80) millisec: %d\n", brake_test_time);
            eve.accel_ratio = get_accelerator_value();
            eve.brake_ratio = get_brake_value();

            if (eve.accel_ratio > 0 || eve.brake_ratio > 0) {
                eve.cruise = DISABLED;
            }

            send_drive_commands();

            drive_timer = sc_get_timer();
            //UART_printf("Accel value = %f, Brake value = %f \n\r", 100*eve.accel_ratio, 100*eve.brake_ratio);
        }

        //MLINK periodic communication
        if (sc_get_timer() > telem_timer + 100) {
            reduce_ws_data();

            //UART_printf("Sending periodic MLINK data, brake_ratio = %d\n", mlink_buffer);
            mlink_buffer = (int)(100*eve.brake_ratio);
            mlink_send(0x00, MLINK_BRAKE_RATIO_ADDR, (void*) &mlink_buffer);

            mlink_buffer = (int)(eve.drive_mode);
            mlink_send(0x00, MLINK_GEAR_ADDR, (void*) &mlink_buffer);

            mlink_buffer = (int)(eve.cruise);
            mlink_send(0x00, MLINK_CRUISE_ADDR, (void*) &mlink_buffer);

            mlink_buffer = (int)(eve.cruise_speed);
            mlink_send(0x00, MLINK_CRUISE_SPEED_ADDR, (void*) &mlink_buffer);

           	mlink_send_ws_reduced(&left_ws_small, MLINK_LHS_MOTOR_OFFSET);
            mlink_send_ws_reduced(&right_ws_small, MLINK_RHS_MOTOR_OFFSET);
            telem_timer = sc_get_timer();
        }

        /* Tickle the watchdog so we don't reset */
        WDT_Feed();
    }
}


//DRIVE MODE
void handler_parkingmode() {
    //scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS, OFFSET_PARKED);
    GPIO_IntClear(PARKINGMODE_PORT, PARKINGMODE_PIN);
    if (GPIO_GetValue(PARKINGMODE_PORT, PARKINGMODE_PIN)) {
        eve.drive_mode = PARKED;
        UART_printf("Parked mode\n");
    }
}
void handler_neutralmode() {
    //scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS, OFFSET_NEUTRAL);
    GPIO_IntClear(NEUTRALMODE_PORT, NEUTRALMODE_PIN);
    if (GPIO_GetValue(NEUTRALMODE_PORT, NEUTRALMODE_PIN)) {
        eve.drive_mode = NEUTRAL;
        UART_printf("Neutral mode\n");
    }

}
void handler_reversemode() {
    //scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS, OFFSET_REVERSE);
    GPIO_IntClear(REVERSEMODE_PORT, REVERSEMODE_PIN);
    if (GPIO_GetValue(REVERSEMODE_PORT, REVERSEMODE_PIN)) {
        eve.drive_mode = REVERSE;
        UART_printf("Reverse mode\n");
    }
}
void handler_drivemode() {
    //scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS, OFFSET_DRIVE);
    GPIO_IntClear(DRIVEMODE_PORT, DRIVEMODE_PIN);
    if (GPIO_GetValue(DRIVEMODE_PORT, DRIVEMODE_PIN)) {
        eve.drive_mode = DRIVE;
        UART_printf("Drive mode\n");
    }
}
void handler_ecomode() {
    //scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS, OFFSET_ECO);
    GPIO_IntClear(ECOMODE_PORT, ECOMODE_PIN);
    if (GPIO_GetValue(ECOMODE_PORT, ECOMODE_PIN)) {
        eve.drive_mode = ECODRIVE;
        //UART_printf("ECO mode\n");
    }
}



/* unfinished code
void handler_parkingmode() {
    state1 = state1 | PARKINGMODE;
    scandal_send_channel(TELEM_LOW,CHANNEL_DRIVERCONTROLS, state1);
}
void handler_neutralmode() {
    state1 = state1 | NEUTRALMODE;
    scandal_send_channel(TELEM_LOW,CHANNEL_DRIVERCONTROLS,NEUTRALMODE);
}
void handler_reversemode() {
    state1 = state1 | REVERSEMODE;
    scandal_send_channel(TELEM_LOW,CHANNEL_DRIVERCONTROLS,REVERSEMODE);
}
void handler_drivemode() {
    state1 = state1 | DRIVEMODE;
    scandal_send_channel(TELEM_LOW,CHANNEL_DRIVERCONTROLS,DRIVEMODE);
}
void handler_ecomode() {
    state1 = state1 | ECOMODE;
    scandal_send_channel(TELEM_LOW,CHANNEL_DRIVERCONTROLS,ECOMODE);
}

*/
void handler_cruise() {
    GPIO_IntClear(CRUISE_TOGGLE_PORT, CRUISE_TOGGLE_PIN);
    eve.cruise = ~eve.cruise;
    UART_printf("Cruise state: %d\n", eve.cruise);
}

void handler_cruise_up() {
    GPIO_IntClear(CRUISE_INCREMENT_PORT, CRUISE_INCREMENT_PIN);
    eve.cruise_speed += 1;
    UART_printf("Cruise Speed: %u\n", eve.cruise_speed);
}
void handler_cruise_down() {
    GPIO_IntClear(CRUISE_DECREMENT_PORT, CRUISE_DECREMENT_PIN);
    eve.cruise_speed -= 1;
    UART_printf("Cruise Speed: %u\n", eve.cruise_speed);
}

void drive_setup_handlers(void) {
    /* Setup interrupt functions for peripherals connected to the drive CAN node */

    GPIO_RegisterInterruptHandler(PARKINGMODE_PORT, PARKINGMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_parkingmode);
    GPIO_RegisterInterruptHandler(NEUTRALMODE_PORT, NEUTRALMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_neutralmode);
    GPIO_RegisterInterruptHandler(REVERSEMODE_PORT, REVERSEMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_reversemode);
    GPIO_RegisterInterruptHandler(DRIVEMODE_PORT, DRIVEMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_drivemode);
    GPIO_RegisterInterruptHandler(ECOMODE_PORT, ECOMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_ecomode);

    /*
    GPIO_RegisterInterruptHandler(PARKINGMODE_PORT, PARKINGMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_parkingmode);
    GPIO_RegisterInterruptHandler(NEUTRALMODE_PORT, NEUTRALMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_neutralmode);
    GPIO_RegisterInterruptHandler(REVERSEMODE_PORT, REVERSEMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_reversemode);
    GPIO_RegisterInterruptHandler(DRIVEMODE_PORT, DRIVEMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_drivemode);
    GPIO_RegisterInterruptHandler(ECOMODE_PORT, ECOMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_ecomode);
    */
    GPIO_RegisterInterruptHandler(CRUISE_TOGGLE_PORT, CRUISE_TOGGLE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, handler_cruise);

    GPIO_RegisterInterruptHandler(CRUISE_INCREMENT_PORT, CRUISE_INCREMENT_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, handler_cruise_up);
    GPIO_RegisterInterruptHandler(CRUISE_DECREMENT_PORT, CRUISE_DECREMENT_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, handler_cruise_down);
    
}


void init_pins()
{
    GPIO_SetDir(PARKINGMODE_PORT, PARKINGMODE_PIN, 0);
    GPIO_SetDir(NEUTRALMODE_PORT, NEUTRALMODE_PIN, 0);
    GPIO_SetDir(REVERSEMODE_PORT, REVERSEMODE_PIN, 0);
    GPIO_SetDir(DRIVEMODE_PORT, DRIVEMODE_PIN, 0);
    GPIO_SetDir(ECOMODE_PORT, ECOMODE_PIN, 0);
}

