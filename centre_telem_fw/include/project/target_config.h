#define RED_LED_PORT 2		// Port for led
#define RED_LED_BIT 8		// Bit on port for led
#define YELLOW_LED_PORT 2		// Port for led
#define YELLOW_LED_BIT 7		// Bit on port for led
#define LED_TOGGLE_TICKS 100 // 100 ticks = 1 Hz flash rate
#define COUNT_MAX		3 // how high to count on the LED display



// Copied from centre_drive_fw's target_config.h by Anmol Thind on 16/05/2015
// Car Gearing States
#define PARKED                  0
#define REVERSE                 1
#define NEUTRAL                 2
#define ECODRIVE                3
#define DRIVE                   4


// Copied from centre_drive_fw's target_config.h by Anmol Thind on 16/05/2015
// Precharge State
#define DISCHARGED              0
#define CHARGED                 1

#define DISABLED                0
#define ENABLED                 1


#define WS_REDUCED_NUM_MEMBERS    11
/* Reduced struct of output wavesculptor data, this should contain
 * everything which needs to be sent to the telemetry network */
typedef struct ws_reduced_t {
    uint32_t    active_motor;
    uint32_t    motor_velocity;
    uint32_t    errors;
    uint32_t    limits;
    uint32_t    bus_current;
    uint32_t    bus_voltage;
    uint32_t    phase_a;
    uint32_t    phase_b;
    uint32_t    motor_temp;
    uint32_t    heatsink_temp;
    uint32_t    odometer;
} ws_reduced_t;

// Copied from centre_drive_fw's target_config.h by Anmol Thind on 16/05/2015
/* Drive State Struct - Holds all info on current drive state of vehicle */
typedef struct drive_state {
    uint8_t     drive_mode;
    uint8_t     regen;
    uint8_t     precharge;
    uint8_t     cruise;
    float       accel_ratio;
    float       brake_ratio;
} drive_state;

#define MISC0_PORT 3
#define MISC0_BIT 2
#define MISC1_PORT 3
#define MISC1_BIT 3

//Header 5
#define HIGHBEAM_PORT               1
#define HIGHBEAM_PIN                4

#define POSITIONLIGHT_PORT          1
#define POSITIONLIGHT_PIN           1

#define IND_RIGHT_PORT              0
#define IND_RIGHT_PIN               6

#define IND_LEFT_PORT               2
#define IND_LEFT_PIN                9

#define LOWBEAM_PORT                0
#define LOWBEAM_PIN                 2


//Header 3
#define IMMOBILISE_PORT             2
#define IMMOBILISE_PIN              7

#define HAZARD_PORT                 1
#define HAZARD_PIN                  9

#define DEMIST_PORT                 2
#define DEMIST_PIN                  5

#define WARNING_NONSPECIFIC_PORT    0
#define WARNING_NONSPECIFIC_PIN     7

#define SEATBELT_SENSOR_PORT        2
#define SEATBELT_SENSOR_PIN         10


//Header 7
#define BRAKEFAIL_PORT      1
#define BRAKEFAIL_PIN       11

#define LIGHTSFAIL_PORT     1
#define LIGHTSFAIL_PIN      2

#define HANDCHECK_PORT      1
#define HANDCHECK_PIN       0

#define CONTROLFAIL_PORT    2
#define CONTROLFAIL_PIN     8

#define HANDBRAKE_PORT	    1
#define HANDBRAKE_PIN       8





// something about Send High Output on telem GPIO 2.7 to turn on Piezo buzzer or otherwise use square wave with normal speaker

#define HORN_PORT                   2
#define SPEAKER_PORT                2
#define SPEAKER_PIN                 10




// Non Assigned
#define LOWFLUID_PORT   4
#define LOWFLUID_PIN	13

#define HORN_PIN                    10


// PORTS


#define WIPER_INT_PORT              1
#define WIPER_LOW_PORT              1
#define WIPER_HIGH_PORT             1
#define WIPER_WASH_PORT             1

#define POSITIONLIGHT_PORT          1
#define LOWBEAM_PORT                0
#define HIGHBEAM_PORT		        1

//MORE PINS



#define WIPER_INT_PIN               0
#define WIPER_LOW_PIN               2
#define WIPER_HIGH_PIN              3
#define WIPER_WASH_PIN              5

#define IND_RIGHT_PIN               6
#define IND_LEFT_PIN                9

#define POSITIONLIGHT_PIN           1
#define LOWBEAM_PIN                 2
#define HIGHBEAM_PIN  		        4



#define CHANNEL_DRIVERCONTROLS		1
#define CHANNEL_HAZARDS				2	
#define CHANNEL_START				3
#define CHANNEL_ON 					4
#define CHANNEL_WS_SPEED            5
#define CHANNEL_CRUISE_SPEED        6
#define CHANNEL_CRUISE_TOGGLE       7

// Command mapping for communication with other CAN nodes (Davina)
#define CHANNEL_CONTROLS_TELEM 		2

// Offsets for 32 bit flags for communicating between center/dash
#define OFFSET_ONOFF                0x1     // Enable (1) or disable (0)

#define OFFSET_INDICATOR_LEFT       0x2
#define OFFSET_INDICATOR_RIGHT      0x4

#define OFFSET_PARKED               0x8
#define OFFSET_REVERSE              0x10
#define OFFSET_NEUTRAL              0x20
#define OFFSET_DRIVE                0x40
#define OFFSET_ECO                  0x80
#define OFFSET_CRUISE_TOGGLE        0x100

#define OFFSET_HANDBRAKE            0x200
#define OFFSET_HANDCHECK            0x400
#define OFFSET_BRAKE_FAIL           0x800
#define OFFSET_LIGHTS_FAIL       0x1000
#define OFFSET_CONTROL_FAILURE      0x2000
#define OFFSET_WARNING_NONSPECIFIC  0x4000

#define OFFSET_LOW_BATTERY          0x8000
#define OFFSET_POSITION_LIGHT       0x10000
#define OFFSET_LOWBEAM              0x20000
#define OFFSET_HIGHBEAM             0x40000
#define OFFSET_SEATBELT_SENSOR      0x80000

#define OFFSET_LOW_FLUID            0x100000
#define OFFSET_HORN                 0x200000
#define OFFSET_HAZARDS              0x400000
#define OFFSET_DEMISTER             0x800000
#define OFFSET_WIPER_INT            0x1000000
#define OFFSET_WIPER_LOW            0x2000000
#define OFFSET_WIPER_HIGH           0x4000000
#define OFFSET_WIPER_WASH           0x8000000

//Eco was flickering because offset braking is the same as eco

#define OFFSET_BRAKING                 0x200000 //Brake Lights for SUTI, its fine to keep this the same as cruise toggle.


// Switchcard addresses for each light quadrant
#define SC_FRONT_LEFT		74
#define SC_FRONT_RIGHT		75
#define SC_REAR_LEFT		76
#define SC_REAR_RIGHT		77

// Front left channel assignments
#define FL_HIGHBEAM			0
#define FL_LOWBEAM			1
#define FL_DAYTIME_RUNNING	2
#define FL_BLINKER			3

// Front right channel assignments
#define FL_HIGHBEAM			0
#define FL_LOWBEAM			1
#define FL_DAYTIME_RUNNING	2
#define FL_BLINKER			3

// Rear left channel assignments
#define RL_REVERSE			0
#define RL_STOP				1
#define RL_NIGHT_RUNNING	2
#define RL_BLINKER			3


// Rear right channel assignments
#define RR_REVERSE			0
#define RR_STOP				1
#define RR_NIGHT_RUNNING	2
#define RR_BLINKER			3

#define WHEEL_DIAMETER      0.56