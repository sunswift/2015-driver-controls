#include <scandal/engine.h>
#include <scandal/message.h>
#include <scandal/leds.h>
#include <scandal/utils.h>
#include <scandal/uart.h>
#include <scandal/stdio.h>
#include <scandal/wdt.h>
#include <scandal/wavesculptor.h>

#include <string.h>

#include <project/driver_config.h>
#include <project/target_config.h>
#include <arch/can.h>
#include <arch/uart.h>
#include <arch/timer.h>
#include <arch/gpio.h>
#include <arch/types.h>
#include <arch/ssp.h>

#include <project/mlink_slave.h>

#define INDICATOR_PERIOD        660
#define START_THRESH_PERIOD     1000

void telem_setup_handlers(void);
void init_pins(void);

int             immobilise_state = 1;

ws_reduced_t    Left_WS_Reduced;
ws_reduced_t    Right_WS_Reduced;
uint32_t        brake_ratio;

void setup() {
    // setup stuff
    GPIO_Init();
    GPIO_SetDir(RED_LED_PORT, RED_LED_BIT, 1);
    GPIO_SetDir(YELLOW_LED_PORT, YELLOW_LED_BIT, 1);
    init_pins();
}

/* Main Function */
int main(void) {

    setup();

    Left_WS_Reduced.motor_velocity = 0;


    /* Initialise the watchdog timer. If the node crashes, it will restart automatically */
    WDT_Init();

    /* Initialise Scandal, registers for the config messages, timesync messages and in channels */
    scandal_init();

    /* Initialise UART0 */
    UART_Init(115200);

    /* Wait until UART is ready */
    scandal_delay(100);

    /* Display welcome header over UART */
    UART_printf("Welcome to the centre console telem project! This is coming out over UART0\n\r");

    /* Set up the GPIO handler */
    telem_setup_handlers();

    //MLINK Initialisation
    mlink_setup();

    sc_time_t one_sec_timer = sc_get_timer();
    sc_time_t start_timer = sc_get_timer();
    sc_time_t ws_sendout_timer = sc_get_timer();

    int timing_start = 0;


    /* This is the main loop, go for ever! */
    while (1) {
        /* handle_scandal checks whether there are pending requests from CAN, and sends a heartbeat message.
         * The heartbeat message encodes some data in the first 4 bytes of the CAN message, such as
         * the number of errors and the version of scandal */
        handle_scandal();

        //DEBUGGING CAN TRANSMISSION ONLY
        /*
        if (sc_get_timer() > one_sec_timer + 100) {
            scandal_send_channel(TELEM_LOW, 5, 11);
            one_sec_timer = sc_get_timer();
            UART_printf("Sent debug CAN msg\n\r");
        }
        */

        // Wavesculptor info sendout loop
        if (sc_get_timer() > ws_sendout_timer + 100) {
            uint32_t vehicle_speed = 1000 * 0.18849 * WHEEL_DIAMETER * Left_WS_Reduced.motor_velocity;
            scandal_send_channel(TELEM_HIGH, CHANNEL_WS_SPEED, vehicle_speed);
            ws_sendout_timer = sc_get_timer();
            //UART_printf("Speed CAN packet sent: %u\n\r", vehicle_speed);
        }

        /* Tickle the watchdog so we don't reset */
        WDT_Feed();
    }
}



//BRAKE RELATED
void handler_handbrake() {
    GPIO_IntClear(HANDBRAKE_PORT, HANDBRAKE_PIN);

    if (GPIO_GetValue(HANDBRAKE_PORT, HANDBRAKE_PIN)) {
        UART_printf("Handbrake on \n\r");
        scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_HANDBRAKE);
    }
    else {
        UART_printf("Handbrake off \n\r");
        scandal_send_channel(TELEM_LOW, CHANNEL_DRIVERCONTROLS,OFFSET_HANDBRAKE);
    }
}
//BRAKE FAIL
void handler_brakefail() {
    GPIO_IntClear(BRAKEFAIL_PORT, BRAKEFAIL_PIN);

    if (GPIO_GetValue(BRAKEFAIL_PORT, BRAKEFAIL_PIN)) {
        UART_printf("Brakes fail on\n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_BRAKE_FAIL);
    }
    else {
        UART_printf("Brakes fail off \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_BRAKE_FAIL);
    }
}

//LOW FLUID
void handler_lowfluid() {
    GPIO_IntClear(LOWFLUID_PORT, LOWFLUID_PIN);

    if (GPIO_GetValue(LOWFLUID_PORT, LOWFLUID_PIN)) {
        UART_printf("Low fluid on \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_LOW_FLUID);
    }
    else {
        UART_printf("Low fluid off \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_LOW_FLUID);
    }
}

//LIGHT FAILURE
void handler_lightsfail() {
    GPIO_IntClear(LIGHTSFAIL_PORT, LIGHTSFAIL_PIN);

    if (GPIO_GetValue(LIGHTSFAIL_PORT, LIGHTSFAIL_PIN)) {
        UART_printf("Lights fail on \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_LIGHTS_FAIL);
    }
    else {
        UART_printf("Lights fail off \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_LIGHTS_FAIL);
    }
}

void handler_controlfail() {
    GPIO_IntClear(CONTROLFAIL_PORT, CONTROLFAIL_PIN);

    if (GPIO_GetValue(CONTROLFAIL_PORT, CONTROLFAIL_PIN)) {
        UART_printf("Control fail on \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_CONTROL_FAILURE);
    }
    else {
        UART_printf("Control fail off \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_CONTROL_FAILURE);
    }
}

void handler_handcheck() {
    GPIO_IntClear(HANDCHECK_PORT, HANDCHECK_PIN);

    if (GPIO_GetValue(HANDCHECK_PORT, HANDCHECK_PIN)) {
        UART_printf("Hand check on \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_HANDCHECK);
    }
    else {
        UART_printf("Hand check off \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_HANDCHECK);
    }
}

//Hazards
void handler_hazards(){
    UART_printf("Hazard toggle detected\n\r");
    scandal_send_channel(TELEM_HIGH, CHANNEL_HAZARDS, 1);
    GPIO_IntClear(HAZARD_PORT, HAZARD_PIN);
}

//Demister
void handler_demister(){
    UART_printf("Demister toggle \n\r");
    scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_DEMISTER);
    GPIO_IntClear(DEMIST_PORT, DEMIST_PIN);
}

//SWITCHCARD RELATED
void handler_ind_left(){
    GPIO_IntClear(IND_LEFT_PORT, IND_LEFT_PIN);
    //UART_printf("In left ind handler \n\r");

    if (GPIO_GetValue(IND_LEFT_PORT, IND_LEFT_PIN)) {
        UART_printf("Left indicator on\n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_INDICATOR_LEFT);
    }
    else {
        UART_printf("Left indicator off\n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_INDICATOR_LEFT);
    }
}
void handler_ind_right(){
    GPIO_IntClear(IND_RIGHT_PORT, IND_RIGHT_PIN);
    //UART_printf("in right ind handler\n\r");

    if (GPIO_GetValue(IND_RIGHT_PORT, IND_RIGHT_PIN)) {
        UART_printf("Right indicator on \n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_INDICATOR_RIGHT);
    }
    else {
        UART_printf("Right indicator off \n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_INDICATOR_RIGHT);
    }
}
void handler_lowbeam(){
    GPIO_IntClear(LOWBEAM_PORT, LOWBEAM_PIN);

    if (GPIO_GetValue(LOWBEAM_PORT, LOWBEAM_PIN)) {
        UART_printf("Low beams on \n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_LOWBEAM);
    }
    else {
        UART_printf("Low beams off\n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_LOWBEAM);
    }
    // Send command to switchcard to turn on lowbeams & rear night running lights
}
void handler_highbeam(){
    GPIO_IntClear(HIGHBEAM_PORT, HIGHBEAM_PIN);

    if(GPIO_GetValue(HIGHBEAM_PORT, HIGHBEAM_PIN)) {
        UART_printf("High beams on\n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_HIGHBEAM);
    }
    else {
        UART_printf("High beams off\n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_HIGHBEAM);
    }
    // Send command to switchcard to turn on highbeams & rear night running lights
}
void handler_positionlight(){
    GPIO_IntClear(POSITIONLIGHT_PORT, POSITIONLIGHT_PIN);

    if (GPIO_GetValue(POSITIONLIGHT_PORT, POSITIONLIGHT_PIN)) {
        UART_printf("Position lights on \n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_POSITION_LIGHT);
    }
    else {
        UART_printf("Position lights off \n\r");
        scandal_send_channel(CONTROL_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_POSITION_LIGHT);
    }
    //Send command to turn on position lights
}

void handler_seatbelt_sensor(){
    GPIO_IntClear(SEATBELT_SENSOR_PORT, SEATBELT_SENSOR_PIN);

    if (GPIO_GetValue(SEATBELT_SENSOR_PORT, SEATBELT_SENSOR_PIN)) {
        UART_printf("Seatbelt in\n");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS,  OFFSET_SEATBELT_SENSOR);
        //GPIO_SetValue(SPEAKER_PORT, SPEAKER_PIN, 0);
    }
    else {
        UART_printf("Seatbelt off\n");
        GPIO_SetValue(SPEAKER_PORT, SPEAKER_PIN, 1);
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_SEATBELT_SENSOR);
    }
}



void handler_warning_nonspecific() {
    GPIO_IntClear(WARNING_NONSPECIFIC_PORT, WARNING_NONSPECIFIC_PIN);

    if (GPIO_GetValue(WARNING_NONSPECIFIC_PORT, WARNING_NONSPECIFIC_PIN)) {
        UART_printf("Warning non-specific on \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_ONOFF | OFFSET_WARNING_NONSPECIFIC);
    }
    else {
        UART_printf("Warning non-specific off \n\r");
        scandal_send_channel(TELEM_HIGH, CHANNEL_DRIVERCONTROLS, OFFSET_WARNING_NONSPECIFIC);
    }
}

void telem_setup_handlers(void) {
        /* Setup interrupts for the peripherals connected to the centre console telemetry node */

    //GPIO_RegisterInterruptHandler(HORN_PORT, HORN_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_horn);

    /*
    GPIO_RegisterInterruptHandler(PARKINGMODE_PORT, PARKINGMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_parkingmode);
    GPIO_RegisterInterruptHandler(NEUTRALMODE_PORT, NEUTRALMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_neutralmode);
    GPIO_RegisterInterruptHandler(REVERSEMODE_PORT, REVERSEMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_reversemode);
    GPIO_RegisterInterruptHandler(DRIVEMODE_PORT, DRIVEMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_drivemode);
    GPIO_RegisterInterruptHandler(ECOMODE_PORT, ECOMODE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_ecomode);
    */

    GPIO_RegisterInterruptHandler(LOWFLUID_PORT, LOWFLUID_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_lowfluid);

    /*Header 5*/
    GPIO_RegisterInterruptHandler(HIGHBEAM_PORT, HIGHBEAM_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_highbeam);
    GPIO_RegisterInterruptHandler(POSITIONLIGHT_PORT, POSITIONLIGHT_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_positionlight);
    GPIO_RegisterInterruptHandler(IND_RIGHT_PORT, IND_RIGHT_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_ind_right);
    GPIO_RegisterInterruptHandler(IND_LEFT_PORT, IND_LEFT_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_ind_left);
    GPIO_RegisterInterruptHandler(LOWBEAM_PORT, LOWBEAM_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_lowbeam);

    /*Header 3*/
    GPIO_RegisterInterruptHandler(HAZARD_PORT, HAZARD_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_hazards);

    //GPIO_RegisterInterruptHandler(DEMIST_PORT, DEMIST_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_demister);

    //GPIO_RegisterInterruptHandler(WIPER_INT_PORT, WIPER_INT_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_wiper_int);
    //GPIO_RegisterInterruptHandler(WIPER_LOW_PORT, WIPER_LOW_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_wiper_low);
    //GPIO_RegisterInterruptHandler(WIPER_HIGH_PORT, WIPER_HIGH_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_wiper_high);
    //GPIO_RegisterInterruptHandler(WIPER_WASH_PORT, WIPER_WASH_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_wiper_wash);

    GPIO_RegisterInterruptHandler(DEMIST_PORT, DEMIST_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_SINGLE_EDGE, GPIO_INTERRUPT_EVENT_RISING, &handler_demister);
    GPIO_RegisterInterruptHandler(SEATBELT_SENSOR_PORT, SEATBELT_SENSOR_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_seatbelt_sensor);
    GPIO_RegisterInterruptHandler(WARNING_NONSPECIFIC_PORT, WARNING_NONSPECIFIC_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_warning_nonspecific);

    /*Header 7*/
    GPIO_RegisterInterruptHandler(BRAKEFAIL_PORT, BRAKEFAIL_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_brakefail);
    GPIO_RegisterInterruptHandler(LIGHTSFAIL_PORT, LIGHTSFAIL_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_lightsfail);
    GPIO_RegisterInterruptHandler(HANDCHECK_PORT, HANDCHECK_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_handcheck);
    GPIO_RegisterInterruptHandler(CONTROLFAIL_PORT, CONTROLFAIL_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_controlfail);
    GPIO_RegisterInterruptHandler(HANDBRAKE_PORT, HANDBRAKE_PIN, GPIO_INTERRUPT_SENSE_EDGE, GPIO_INTERRUPT_DOUBLE_EDGE, GPIO_INTERRUPT_EVENT_NONE, &handler_handbrake);



}

void init_pins()
{


    /*
    GPIO_SetDir(PARKINGMODE_PORT, PARKINGMODE_PIN, 0);
    GPIO_SetDir(NEUTRALMODE_PORT, NEUTRALMODE_PIN, 0);
    GPIO_SetDir(REVERSEMODE_PORT, REVERSEMODE_PIN, 0);
    GPIO_SetDir(DRIVEMODE_PORT, DRIVEMODE_PIN, 0);
    GPIO_SetDir(ECOMODE_PORT, ECOMODE_PIN, 0);
    */



    /*
    GPIO_SetDir(LOWFLUID_PORT, LOWFLUID_PIN, 0);

    */

    GPIO_SetDir(HAZARD_PORT, HAZARD_PIN, 0);
    //GPIO_SetDir(DEMIST_PORT, DEMIST_PIN, 0);
    //GPIO_SetDir(WIPER_INT_PORT, WIPER_INT_PIN, 0);
    //GPIO_SetDir(WIPER_LOW_PORT, WIPER_LOW_PIN, 0);
    //GPIO_SetDir(WIPER_HIGH_PORT, WIPER_HIGH_PIN, 0);
    //GPIO_SetDir(WIPER_WASH_PORT, WIPER_WASH_PIN, 0);

    /*Header 5*/
    GPIO_SetDir(IND_RIGHT_PORT, IND_RIGHT_PIN, 0);
    GPIO_SetDir(IND_LEFT_PORT, IND_LEFT_PIN, 0);
    GPIO_SetDir(POSITIONLIGHT_PORT, POSITIONLIGHT_PIN, 0);
    GPIO_SetDir(LOWBEAM_PORT, LOWBEAM_PIN, 0);
    GPIO_SetDir(HIGHBEAM_PORT, HIGHBEAM_PIN, 0);

    /*Header 3*/
    GPIO_SetDir(HAZARD_PORT, HAZARD_PIN, 0);
    GPIO_SetDir(DEMIST_PORT, DEMIST_PIN, 0);
    GPIO_SetDir(SEATBELT_SENSOR_PORT, SEATBELT_SENSOR_PIN, 0);
    GPIO_SetDir(WARNING_NONSPECIFIC_PORT, WARNING_NONSPECIFIC_PIN, 0);

    /*Header 7*/
    GPIO_SetDir(BRAKEFAIL_PORT, BRAKEFAIL_PIN, 0);
    GPIO_SetDir(LIGHTSFAIL_PORT, LIGHTSFAIL_PIN, 0);
    GPIO_SetDir(HANDCHECK_PORT, HANDCHECK_PIN, 0);
    GPIO_SetDir(CONTROLFAIL_PORT, CONTROLFAIL_PIN, 0);
    GPIO_SetDir(HANDBRAKE_PORT, HANDBRAKE_PIN, 0);
}

